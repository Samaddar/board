from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from .models import Board

# Create your views here.
def home(request):
    boards = Board.objects.all()
    return render(request,'home.html',{'boards':boards})
    """boards_names = list()

    for board in boards:
       boards_names.append(board.name)
    
    response_html = '<br>'.join(boards_names)

    return HttpResponse(response_html)"""

def about(request):
    # do something...
    return render(request, 'about.html')

def about_company(request):
    # do something else...
    # return some data along with the view...
    return render(request, 'about_company.html', {'company_name': 'Reliance Finance Limited.'})

def board_topics(request, pk):
    board = Board.objects.get(pk=pk)
    return render(request, 'topics.html',{'board' : board})